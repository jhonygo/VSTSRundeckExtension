#[CmdletBinding()]
param(
  # VARIABLES TAKEN FROM THE VSTS EXTENSION
  # TODO : add control on mandatory options, i.e.  [string] [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $URL,
  [string] [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $URL,
  [string] [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $token,
  [string] [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $jobid,
  [string] $jobOptions,
  [string] $runOptions,
  # CONSTANTS
  [string]$command = "run",
  [string]$rundeckCLI = ((Get-Item -Path ".\" -Verbose).FullName + "\runtime\rundeck-cli.jar")
)

#try {
	# Define the full command to execGet-ChildItem -Path ute
	# [string] $printableCommand = "$jobOptions"

    # Trying to set path to java through JAVA_HOME first to reuse maven/sonarqube setups
    [string]$runtime = ''
    if (${ENV:JAVA_HOME}) {
        if (Test-Path "${ENV:JAVA_HOME}\bin\java.exe") {
        $runtime = ("${ENV:JAVA_HOME}\bin\java.exe")
        } else {
            Write-Host "WARNING : JAVA_HOME set to $ENV:JAVA_HOME but java executable NOT found there"
        }
    }
    # Location of java.exe not resolved through JAVA_HOME : fall back to the system path
    if ($runtime) {
        Write-Host "INFO : java executable found in JAVA_HOME ($ENV:JAVA_HOME)"
    } else {
        Write-Host "WARNING : trying to find java executable through system path..."
        if (where.exe "java.exe") { 
            # find matches ... let's take the first (defaut) one"
            $runtime = (where.exe "java.exe" | Select -First 1)
            Write-Host "INFO : java executable found in system path at $runtime"
        } else {
            # no match ... game over
            Write-Host "ERROR : no java executable could be detected. Exiting..."
            Exit 1
        }
    }

        $ENV:RD_URL=$URL
        $ENV:RD_TOKEN=$token
        
        if (${jobOptions}) {
            $jobOptions = " -- ${jobOptions}"
        }
        [string] $printableCommand = "`"$runtime`" -jar `"$rundeckCLI`" $command -i `"$jobid`" $runOptions $jobOptions 2>&1" 
        #$runDeckCommand = @("$command", "-i", "$jobid", "$runOptions", "$jobOptions")
        
    	#Write-Host "Executing $printableCommand"
    	[string]$runCommandScript =  ((Get-Item -Path ".\" -Verbose).FullName + '\runCommand.bat')
    	"@echo off" | Set-Content $runCommandScript
    	"SET RD_URL=$URL" | Add-Content $runCommandScript
    	"SET RD_TOKEN=$token" | Add-Content $runCommandScript
    	$printableCommand | Add-Content $runCommandScript 
	"set errorcode=%ERRORLEVEL%" | Add-Content $runCommandScript 
	":echo %errorcode%" | Add-Content $runCommandScript 
	"exit %errorcode%" | Add-Content $runCommandScript 

    	#Write-Host " --- Check presence of $runCommandScript "
    	#Get-ChildItem -Path ((Get-Item -Path ".\" -Verbose).FullName)
    	
    	#Write-Host " --- Get content  of $runCommandScript "
        #Get-Content $runCommandScript
    	
    	& $runCommandScript 2>&1
	$return=$?	
	if ($return) {
	Write-Host " ---- Deployment successfull"
	} else {

	  throw " ---- Deployment failed: Error trying to do a Rundeck task"
	}
	

#} finally {}
