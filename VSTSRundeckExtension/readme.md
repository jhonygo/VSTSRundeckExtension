# Rundeck extension for TFS/VSTS

### Purpose  
  
This extension aims to provide build/release tasks for Rundeck command-line interface.  
This is a first version acting as a wrapper, without much semantic logic. Feel free to contribute if you have a better plan : https://gitlab.com/jhonygo/VSTSRundeckExtension  
  
### Introduction to Rundeck tool  
Rundeck is a remote task execution tool. Here are some key features:  
  
- Open source solution : Apache 2.0 license, commercial support available [here](http://rundeck.com/)  
- Agentless architecture : relies on OS native interfaces (ssh/scp/sftp, winrm/cifs)   
- Plenty of existing modules for major technologies : [official list](http://rundeck.org/plugins/index.html)  
- Extensible model : some community extensions available [here](https://github.com/rundeck-plugins/)  
  
For details see [rundeck homepage](http://www.rundeck.org).  
  
### Main logic  
  
This extension contains the [rundeck CLI](https://github.com/rundeck/rundeck-cli) package to to interact with Rundeck servers.  
It allows to choose an action taken from a list of available "rd" commands and options. Some additional options can be defined if needed.  
  
### Some pointers  
- [rundeck CLI](https://github.com/rundeck/rundeck-cli)  
- [rundeck documentation](http://rundeck.org/docs/tutorials/)  
- [official plugins](http://rundeck.org/plugins/index.html)  
- [community plugins](https://github.com/rundeck-plugins/)  
  